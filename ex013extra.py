##Esse exercício é de um produto que tem desconto de 10% de pago a vista e 8% de acréscimo se pago a prazo
pro = float(input('Qual o valor do produto? R$'))
vis = (pro * 10 / 100)
praz = (pro * 8 / 100)
print(f'O produto com 10% de desconto fica no valor de R${pro - vis:.2f} sendo pago a vista.'
      f'\n Ou se decidir pagar a prazo sai por R${pro + praz:.2f}')