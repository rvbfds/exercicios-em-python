numero = int(input('Escreva o seu número preferido:'))
print(f'O dobro do seu número é {numero * 2}, o triplo é {numero * 3} e a raiz quadrada é {numero**(1/2):.3f}!')
